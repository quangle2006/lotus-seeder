CREATE OR REPLACE FUNCTION next_id(OUT result bigint, seq text) AS $$
DECLARE
    our_epoch bigint := 1600646400000;
    seq_id bigint;
    now_millis bigint;
    shard_id int := 5;
BEGIN
    SELECT nextval(seq) % 1024 INTO seq_id;
    SELECT FLOOR(EXTRACT(EPOCH FROM clock_timestamp())) INTO now_millis;
    result := (now_millis - our_epoch)*1000 << 23;
    result := result | (shard_id <<10);
    result := result | (seq_id);    
END;
    $$ LANGUAGE PLPGSQL;

CREATE SEQUENCE users_id_seq;
CREATE TABLE "users" (
  "id" bigint not null default next_id('users_id_seq') primary key,
  "username" text,
  "password" text,
  "email" text,
  "fullname" text,
  "phone_number" text,
  "address" text,
  "avatar" text,
  "type" int,
  "height" int,
  "weight" int,
  "birthday" timestamp
);

CREATE SEQUENCE trainer_registrations_id_seq;
CREATE TABLE "trainer_registrations" (
  "id" bigint not null default next_id('trainer_registrations_id_seq') primary key,
  "user_id" bigint,
  "certificate_link" text
);

CREATE SEQUENCE courses_id_seq;
CREATE TABLE "courses" (
  "id" bigint not null default next_id('courses_id_seq') primary key,
  "name" text,
  "description" text,
  "duration" text,
  "address" text,
  "start_time" timestamp,
  "end_time" timestamp,
  "fee" int,
  "max_student" int,
  "user_id" bigint,
  "status" int
);

CREATE SEQUENCE schedules_id_seq;
CREATE TABLE "schedules" (
  "id" bigint not null default next_id('schedules_id_seq') primary key,
  "course_id" bigint,
  "user_id" bigint,
  "at_time" timestamp
);

CREATE SEQUENCE exercises_id_seq;
CREATE TABLE "exercises" (
  "id" bigint not null default next_id('exercises_id_seq') primary key,
  "name" text,
  "description" text,
  "duration" int,
  "contraindication" text,
  "avatar" text,
  "video" text
);

CREATE SEQUENCE tutorials_id_seq;
CREATE TABLE "tutorials" (
  "id" bigint not null default next_id('tutorials_id_seq') primary key,
  "content" text,
  "priority" int,
  "exercise_id" bigint
);

CREATE SEQUENCE schedule_exercises_id_seq;
CREATE TABLE "schedule_exercises" (
  "id" bigint not null default next_id('schedule_exercises_id_seq') primary key,
  "schedule_id" bigint,
  "exercise_id" bigint
);

CREATE SEQUENCE nutritions_id_seq;
CREATE TABLE "nutritions" (
  "id" bigint not null default next_id('nutritions_id_seq') primary key,
  "name" text,
  "description" text,
  "energy" int,
  "avatar" text,
  "amount" int,
  "unit" text
);

CREATE SEQUENCE schedule_nutritions_id_seq;
CREATE TABLE "schedule_nutritions" (
  "id" bigint not null default next_id('schedule_nutritions_id_seq') primary key,
  "schedule_id" bigint,
  "nutrition_id" bigint
);

CREATE SEQUENCE nutrition_vitamins_id_seq;
CREATE TABLE "nutrition_vitamins" (
  "id" bigint not null default next_id('nutrition_vitamins_id_seq') primary key,
  "nutrition_id" bigint,
  "vitamin" int
);

CREATE SEQUENCE nutrition_chemical_compositions_id_seq;
CREATE TABLE "nutrition_chemical_compositions" (
  "id" bigint not null default next_id('nutrition_chemical_compositions_seq') primary key,
  "nutrition_id" bigint,
  "amount" int,
  "unit" text
);

CREATE SEQUENCE categories_id_seq;
CREATE TABLE "categories" (
  "id" bigint not null default next_id('categories_id_seq') primary key,
  "name" text,
  "avatar" text
);

CREATE SEQUENCE course_categories_id_seq;
CREATE TABLE "course_categories" (
  "id" bigint not null default next_id('course_categories_id_seq') primary key,
  "course_id" bigint,
  "category_id" bigint
);

CREATE SEQUENCE body_parts_id_seq;
CREATE TABLE "body_parts" (
  "id" bigint not null default next_id('body_parts_id_seq') primary key,
  "name" text,
  "avatar" text
);

CREATE SEQUENCE exercise_body_parts_id_seq;
CREATE TABLE "exercise_body_parts" (
  "id" bigint not null default next_id('exercise_body_parts_id_seq') primary key,
  "exercise_id" bigint,
  "body_part_id" bigint
);

CREATE SEQUENCE course_body_parts_id_seq;
CREATE TABLE "course_body_parts" (
  "id" bigint not null default next_id('course_body_parts_id_seq') primary key,
  "course_id" bigint,
  "body_part_id" bigint
);

CREATE SEQUENCE feedbacks_id_seq;
CREATE TABLE "feedbacks" (
  "id" bigint not null default next_id('feedbacks_id_seq') primary key,
  "user_id" bigint,
  "course_id" bigint,
  "schedule_id" bigint,
  "content" text
);

CREATE SEQUENCE notifications_id_seq;
CREATE TABLE "notifications" (
  "id" bigint not null default next_id('notifications_id_seq') primary key,
  "title" text,
  "content" text,
  "receiver_id" bigint,
  "sender_id" bigint
);

CREATE SEQUENCE reports_id_seq;
CREATE TABLE "reports" (
  "id" bigint not null default next_id('reports_id_seq') primary key,
  "user_id" bigint,
  "reported_user_id" bigint,
  "content" text
);

CREATE SEQUENCE course_images_id_seq;
CREATE TABLE "course_images" (
  "id" bigint not null default next_id('course_images_id_seq') primary key,
  "course_id" bigint,
  "image" text
);

CREATE SEQUENCE foods_id_seq;
CREATE TABLE "foods" (
  "id" bigint not null default next_id('foods_id_seq') primary key,
  "name" text,
  "avatar" text,
  "description" text,
  "amount" int,
  "unit" text
);

CREATE SEQUENCE course_foods_id_seq;
CREATE TABLE "course_foods" (
  "id" bigint not null default next_id('course_foods_id_seq') primary key,
  "course_id" bigint,
  "food_id" bigint
);

CREATE SEQUENCE food_nutritions_id_seq;
CREATE TABLE "food_nutritions" (
  "id" bigint not null default next_id('food_nutritions_id_seq') primary key,
  "food_id" bigint,
  "nutrition_id" bigint
);

CREATE SEQUENCE follows_id_seq;
CREATE TABLE "follows" (
  "id" bigint not null default next_id('follows_id_seq') primary key,
  "user_id" bigint,
  "category_id" bigint
);

ALTER TABLE "trainer_registrations" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "courses" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "schedules" ADD FOREIGN KEY ("course_id") REFERENCES "courses" ("id");

ALTER TABLE "schedules" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "tutorials" ADD FOREIGN KEY ("exercise_id") REFERENCES "exercises" ("id");

ALTER TABLE "schedule_exercises" ADD FOREIGN KEY ("schedule_id") REFERENCES "schedules" ("id");

ALTER TABLE "schedule_exercises" ADD FOREIGN KEY ("exercise_id") REFERENCES "exercises" ("id");

ALTER TABLE "schedule_nutritions" ADD FOREIGN KEY ("schedule_id") REFERENCES "schedules" ("id");

ALTER TABLE "schedule_nutritions" ADD FOREIGN KEY ("nutrition_id") REFERENCES "nutritions" ("id");

ALTER TABLE "nutrition_vitamins" ADD FOREIGN KEY ("nutrition_id") REFERENCES "nutritions" ("id");

ALTER TABLE "nutrition_chemical_compositions" ADD FOREIGN KEY ("nutrition_id") REFERENCES "nutritions" ("id");

ALTER TABLE "course_categories" ADD FOREIGN KEY ("course_id") REFERENCES "courses" ("id");

ALTER TABLE "course_categories" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");

ALTER TABLE "exercise_body_parts" ADD FOREIGN KEY ("exercise_id") REFERENCES "exercises" ("id");

ALTER TABLE "exercise_body_parts" ADD FOREIGN KEY ("body_part_id") REFERENCES "body_parts" ("id");

ALTER TABLE "course_body_parts" ADD FOREIGN KEY ("course_id") REFERENCES "courses" ("id");

ALTER TABLE "course_body_parts" ADD FOREIGN KEY ("body_part_id") REFERENCES "body_parts" ("id");

ALTER TABLE "feedbacks" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "feedbacks" ADD FOREIGN KEY ("course_id") REFERENCES "courses" ("id");

ALTER TABLE "feedbacks" ADD FOREIGN KEY ("schedule_id") REFERENCES "schedules" ("id");

ALTER TABLE "notifications" ADD FOREIGN KEY ("receiver_id") REFERENCES "users" ("id");

ALTER TABLE "notifications" ADD FOREIGN KEY ("sender_id") REFERENCES "users" ("id");

ALTER TABLE "course_images" ADD FOREIGN KEY ("course_id") REFERENCES "courses" ("id");

ALTER TABLE "course_foods" ADD FOREIGN KEY ("course_id") REFERENCES "courses" ("id");

ALTER TABLE "course_foods" ADD FOREIGN KEY ("food_id") REFERENCES "foods" ("id");

ALTER TABLE "food_nutritions" ADD FOREIGN KEY ("food_id") REFERENCES "foods" ("id");

ALTER TABLE "food_nutritions" ADD FOREIGN KEY ("nutrition_id") REFERENCES "nutritions" ("id");

ALTER TABLE "follows" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "follows" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");
