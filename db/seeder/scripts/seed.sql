INSERT INTO "users"(id, username,password,email,fullname,phone_number,address,avatar,type,height,weight, birthday) VALUES
 (1, 'nguyena','873b6e802ef','nguyenvana@gmail.com','Nguyễn Văn A',NULL,NULL,NULL,1,170,70,NULL),
 (2, 'nguyenb','873b6e802ef','nguyenvanb@gmail.com','Nguyễn Văn B',NULL,NULL,NULL,1,170,70,NULL),
 (3, 'nguyenc','873b6e802ef','nguyenvanc@gmail.com','Nguyễn Văn C',NULL,NULL,NULL,1,170,70,NULL),
 (4, 'nguyend','873b6e802ef','nguyenvand@gmail.com','Nguyễn Văn D',NULL,NULL,NULL,1,170,70,NULL),
 (5, 'nguyene','873b6e802ef','nguyenvane@gmail.com','Nguyễn Văn E',NULL,NULL,NULL,1,170,70,NULL),
 (6, 'nguyenf','873b6e802ef','nguyenvanf@gmail.com','Nguyễn Văn F',NULL,NULL,NULL,1,170,70,NULL),
 (7, 'nguyeng','873b6e802ef','nguyenvang@gmail.com','Nguyễn Văn G',NULL,NULL,NULL,1,170,70,NULL),
 (8, 'nguyenh','873b6e802ef','nguyenvanh@gmail.com','Nguyễn Văn H',NULL,NULL,NULL,1,170,70,NULL),
 (9, 'nguyenk','873b6e802ef','nguyenvanj@gmail.com','Nguyễn Văn K',NULL,NULL,NULL,1,170,70,NULL),
 (10, 'nguyenj','873b6e802ef','nguyenvank@gmail.com','Nguyễn Văn J',NULL,NULL,NULL,1,170,70,NULL);

INSERT INTO "trainer_registrations"(id, user_id,certificate_link) VALUES
 (1, 1,'https://image.shutterstock.com/image-vector/gift-certificate-template-mandala-ornament-600w-504024880.jpg'),
 (2, 2,'https://image.shutterstock.com/image-vector/gift-certificate-template-mandala-ornament-600w-504024880.jpg'),
 (3, 3,'https://image.shutterstock.com/image-vector/gift-certificate-template-mandala-ornament-600w-504024880.jpg'),
 (4, 4,'https://image.shutterstock.com/image-vector/gift-certificate-template-mandala-ornament-600w-504024880.jpg'),
 (5, 5,'https://image.shutterstock.com/image-vector/gift-certificate-template-mandala-ornament-600w-504024880.jpg');

INSERT INTO "courses"(id, name,description,duration,address,start_time,end_time,fee,max_student,user_id,status) VALUES
 (1, 'Khóa tập Yoga cấp 1','Khóa học dành cho mọi người','60h','222 Điện Biên Phủ Quận 3 TPHCM','2020-12-01','2021-01-01',500000,5,1,0),
 (2, 'Khóa tập Yoga cấp 2','Khóa học dành cho mọi người','60h','223 Điện Biên Phủ Quận 3 TPHCM','2020-11-01','2021-02-01',600000,5,2,0),
 (3, 'Khóa tập Yoga cấp 3','Khóa học dành cho mọi người','60h','224 Điện Biên Phủ Quận 3 TPHCM','2020-12-24','2021-01-10',700000,6,3,0),
 (4, 'Khóa tập Yoga cấp 4','Khóa học dành cho mọi người','60h','225 Điện Biên Phủ Quận 3 TPHCM','2020-12-01','2021-01-01',800000,6,4,0),
 (5, 'Khóa tập Yoga cấp 5','Khóa học dành cho mọi người','60h','226 Điện Biên Phủ Quận 3 TPHCM','2020-12-01','2021-01-01',900000,4,5,0);

INSERT INTO "schedules"(id, course_id, user_id, at_time) VALUES
 (1, 1, 1, '2020-12-01'),
 (2, 2, 2, '2020-11-01'),
 (3, 3, 3, '2020-12-24'),
 (4, 4, 4, '2020-12-01'),
 (5, 5, 5, '2020-12-01');

INSERT INTO "exercises"(id, name, description, duration, contraindication, avatar, video) VALUES
 (1, 'Bài tập hít thở', 'Giúp tăng khả năng tập trung', 30, 'test', 'https://img.freepik.com/free-vector/women-yoga-character-pose-healthy_40876-1199.jpg?size=626&ext=jpg', 'https://www.youtube.com/watch?v=pr6nKdtu73A'),
 (2, 'Bài tập hít thở 2', 'Giúp tăng khả năng tập trung 2', 40, 'test', 'https://img.freepik.com/free-vector/women-yoga-character-pose-healthy_40876-1199.jpg?size=626&ext=jpg', 'https://www.youtube.com/watch?v=pr6nKdtu73A'),
 (3, 'Bài tập hít thở 3', 'Giúp tăng khả năng tập trung 3', 30, 'test', 'https://img.freepik.com/free-vector/women-yoga-character-pose-healthy_40876-1199.jpg?size=626&ext=jpg', 'https://www.youtube.com/watch?v=pr6nKdtu73A'),
 (4, 'Bài tập hít thở 4', 'Giúp tăng khả năng tập trung 4', 30, 'test', 'https://img.freepik.com/free-vector/women-yoga-character-pose-healthy_40876-1199.jpg?size=626&ext=jpg', 'https://www.youtube.com/watch?v=pr6nKdtu73A'),
 (5, 'Bài tập hít thở 5', 'Giúp tăng khả năng tập trung 5', 30, 'test', 'https://img.freepik.com/free-vector/women-yoga-character-pose-healthy_40876-1199.jpg?size=626&ext=jpg', 'https://www.youtube.com/watch?v=pr6nKdtu73A');

INSERT INTO "tutorials"(id, content, priority, exercise_id) VALUES
 (1, 'Bài tập chi tiết khóa 1', 1, 1),
 (2, 'Bài tập chi tiết khóa 2', 1, 2),
 (3, 'Bài tập chi tiết khóa 3', 1, 3),
 (4, 'Bài tập chi tiết khóa 4', 1, 4),
 (5, 'Bài tập chi tiết khóa 5', 1, 5);

INSERT INTO "schedule_exercises"(id, schedule_id, exercise_id) VALUES
 (1, 1, 1),
 (2, 2, 2),
 (3, 3, 3),
 (4, 4, 4),
 (5, 5, 5);

INSERT INTO "nutritions"(id, name, description, energy, avatar, amount, unit) VALUES
 (1, 'Tinh bột', 'Tinh bộ abc', 400, 'https://www.sunriseseniorliving.com/~/media/blog-images/march-2015/6-diseases-that-proper-nutrition-prevents_379_40044310_0_14113110_728.jpg', 2, 'gram'),
 (2, 'Bột yến mạch', 'Bột yến mạch abc', 500, 'https://www.sunriseseniorliving.com/~/media/blog-images/march-2015/6-diseases-that-proper-nutrition-prevents_379_40044310_0_14113110_728.jpg', 3, 'gram'),
 (3, 'Táo đỏ', 'Táo đỏ abc', 600, 'https://www.sunriseseniorliving.com/~/media/blog-images/march-2015/6-diseases-that-proper-nutrition-prevents_379_40044310_0_14113110_728.jpg', 4, 'qua'),
 (4, 'Táo xanh', 'Táo xanh xyz', 500, 'https://www.sunriseseniorliving.com/~/media/blog-images/march-2015/6-diseases-that-proper-nutrition-prevents_379_40044310_0_14113110_728.jpg', 4, 'qua'),
 (5, 'Chuối', 'Chuối abc', 500, 'https://www.sunriseseniorliving.com/~/media/blog-images/march-2015/6-diseases-that-proper-nutrition-prevents_379_40044310_0_14113110_728.jpg', 4, 'qua');

INSERT INTO "schedule_nutritions"(id, schedule_id, nutrition_id) VALUES
 (1, 1, 1),
 (2, 2, 2),
 (3, 3, 3),
 (4, 4, 4),
 (5, 5, 5);

INSERT INTO "nutrition_vitamins"(id, nutrition_id, vitamin) VALUES
 (1, 1, 100),
 (2, 2, 200),
 (3, 3, 300),
 (4, 4, 400),
 (5, 5, 500);

INSERT INTO "nutrition_chemical_compositions"(id, nutrition_id, amount, unit) VALUES
 (1, 1, 10, 'gram'),
 (2, 2, 20, 'gram'),
 (3, 3, 30, 'qua'),
 (4, 4, 40, 'qua'),
 (5, 5, 50, 'qua');

INSERT INTO "categories"(id, name, avatar) VALUES
 (1, 'vitamin A', 'https://image.shutterstock.com/image-photo/red-apple-on-white-background-260nw-158989157.jpg'),
 (2, 'vitamin B', 'https://image.shutterstock.com/image-photo/red-apple-on-white-background-260nw-158989157.jpg'),
 (3, 'vitamin E', 'https://image.shutterstock.com/image-photo/red-apple-on-white-background-260nw-158989157.jpg'),
 (4, 'vitamin B1', 'https://image.shutterstock.com/image-photo/red-apple-on-white-background-260nw-158989157.jpg'),
 (5, 'vitamin C', 'https://image.shutterstock.com/image-photo/red-apple-on-white-background-260nw-158989157.jpg');

INSERT INTO "course_categories"(id, course_id, category_id) VALUES
 (1, 1, 1),
 (2, 2, 2),
 (3, 3, 3),
 (4, 4, 4),
 (5, 5, 5);

INSERT INTO "body_parts"(id, name, avatar) VALUES
 (1, 'tay', ''),
 (2, 'chân', ''),
 (3, 'cơ bụng', ''),
 (4, 'cơ chân', ''),
 (5, 'cổ', '');

INSERT INTO "exercise_body_parts"(id, exercise_id, body_part_id) VALUES
 (1, 1, 1),
 (2, 2, 2),
 (3, 3, 3),
 (4, 4, 4),
 (5, 5, 5);

INSERT INTO "course_body_parts"(id, course_id, body_part_id) VALUES
 (1, 1, 1),
 (2, 2, 2),
 (3, 3, 3),
 (4, 4, 4),
 (5, 5, 5);

INSERT INTO "feedbacks"(id, user_id, course_id, schedule_id, content) VALUES
 (1, 6, 1, 1, 'Khóa học phù hợp với người mới'),
 (2, 7, 2, 2, 'Khóa học chất lượng'),
 (3, 8, 3, 3, 'Giảng viên tận tâm'),
 (4, 9, 4, 4, 'Khóa học ok'),
 (5, 10, 5, 5, 'Khóa học ok');

INSERT INTO "notifications"(id, title, content, receiver_id, sender_id) VALUES
 (1, 'Thông báo đã dăng kí Khóa học', 'Thông báo đã dăng kí Khóa học', 1, 6),
 (2, 'Thông báo đã dăng kí Khóa học', 'Thông báo đã dăng kí Khóa học', 2, 7),
 (3, 'Thông báo đã dăng kí Khóa học', 'Thông báo đã dăng kí Khóa học', 3, 8),
 (4, 'Thông báo đã dăng kí Khóa học', 'Thông báo đã dăng kí Khóa học', 4, 9),
 (5, 'Thông báo đã dăng kí Khóa học', 'Thông báo đã dăng kí Khóa học', 5, 10);

INSERT INTO "reports"(id, user_id, reported_user_id, content) VALUES
 (1, 6, 1, 'Test report'),
 (2, 7, 2, 'Test report'),
 (3, 8, 3, 'Test report'),
 (4, 9, 4, 'Test report'),
 (5, 10, 5, 'Test report');

INSERT INTO "course_images"(id, course_id, image) VALUES
 (1, 1, 'https://miro.medium.com/max/960/1*brn170NjJTiE3Gz7uQIyug.jpeg'),
 (2, 2, 'https://miro.medium.com/max/960/1*brn170NjJTiE3Gz7uQIyug.jpeg'),
 (3, 3, 'https://miro.medium.com/max/960/1*brn170NjJTiE3Gz7uQIyug.jpeg'),
 (4, 4, 'https://miro.medium.com/max/960/1*brn170NjJTiE3Gz7uQIyug.jpeg'),
 (5, 5, 'https://miro.medium.com/max/960/1*brn170NjJTiE3Gz7uQIyug.jpeg');

INSERT INTO "foods"(id, name, avatar, description, amount, unit) VALUES
 (1, 'Phở bò', 'https://img-global.cpcdn.com/recipes/6824738c264d979d/1200x630cq70/photo.jpg', 'Phở thơm ngon', 1, 'tô'),
 (2, 'Phở tái', 'https://img-global.cpcdn.com/recipes/6824738c264d979d/1200x630cq70/photo.jpg', 'Phở thơm ngon', 1, 'tô'),
 (3, 'Phở nạm', 'https://img-global.cpcdn.com/recipes/6824738c264d979d/1200x630cq70/photo.jpg', 'Phở thơm ngon', 1, 'tô'),
 (4, 'Phở gầu gân', 'https://img-global.cpcdn.com/recipes/6824738c264d979d/1200x630cq70/photo.jpg', 'Phở thơm ngon', 1, 'tô'),
 (5, 'Phở đặc biệt', 'https://img-global.cpcdn.com/recipes/6824738c264d979d/1200x630cq70/photo.jpg', 'Phở thơm ngon', 2, 'tô');

INSERT INTO "course_foods"(id, course_id, food_id) VALUES
 (1, 1, 1),
 (2, 2, 2),
 (3, 3, 3),
 (4, 4, 4),
 (5, 5, 5);

INSERT INTO "food_nutritions"(id, food_id, nutrition_id) VALUES
 (1, 1, 1),
 (2, 2, 2),
 (3, 3, 3),
 (4, 4, 4),
 (5, 5, 5);

INSERT INTO "follows"(id, user_id, category_id) VALUES
 (1, 1, 1),
 (2, 2, 2),
 (3, 3, 3),
 (4, 4, 4),
 (5, 5, 5);









