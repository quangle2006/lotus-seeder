package seeder

import (
	"io/ioutil"
	"path"
	"runtime"
)

// Seed to insert data
func (s Seed) Seed() {
	q, err := ioutil.ReadFile(GetSourcePath() + "/scripts/seed.sql")
	if err != nil {
		panic(err)
	}

	_, err = s.db.Exec(string(q))
	if err != nil {
		panic(err)
	}
}

func GetSourcePath() string {
	_, filename, _, _ := runtime.Caller(1)
	return path.Dir(filename)
}
