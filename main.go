package main

import (
	"database/sql"
	"flag"
	"fmt"
	"log"
	"lotus-seeder/db/seeder"
	"os"

	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "root"
	password = "secret"
	dbname   = "lotusfitness"
)

func main() {
	handleArgs()
}

func handleArgs() {
	flag.Parse()
	args := flag.Args()

	if len(args) >= 1 {
		switch args[0] {
		case "seed":
			connString := fmt.Sprintf("host=%s port=%d user=%s "+
				"password=%s dbname=%s sslmode=disable",
				host, port, user, password, dbname)
			// connect DB
			db, err := sql.Open("postgres", connString)
			if err != nil {
				log.Fatalf("Error opening DB: %v", err)
			}
			seeder.Execute(db, args[1:]...)
			os.Exit(0)
		}
	}
}
