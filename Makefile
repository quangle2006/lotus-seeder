postgres:
	docker run --name postgres12 -p 5432:5432 -e POSTGRES_USER=root -e POSTGRES_PASSWORD=secret -d postgres:12-alpine

createdb:
	docker exec -it postgres12 createdb --username=root --owner=root lotusfitness

dropdb:
	docker exec -it postgres12 dropdb lotusfitness

migrateup:
	migrate -path db/migration -database "postgresql://root:secret@localhost:5432/lotusfitness?sslmode=disable" -verbose up

migratedown:
	migrate -path db/migration -database "postgresql://root:secret@localhost:5432/lotusfitness?sslmode=disable" -verbose down

seed:
	go run main.go seed Seed

.PHONY: postgres createdb dropdb migrateup migratedown seed